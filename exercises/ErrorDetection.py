
number_used = int(input("What should the number be: "))
check_bit = int(input("What should the check bit be: "))
trial = bin(number_used)


def countones(num):
    num = str(num)
    #print(num)
    digits = []
    for digit in num:
        digits.append(digit)
    length = len(digits)
    #print(digits)
    i = 0
    count = 0
    while length > 0:
        if digits[i] =='1':
            count += 1
        i += 1
        length -= 1
    if count % 2 == 0:
        if check_bit == 0:
            print("Valid")
        else:
            print("Corrupt")
    else:
        if check_bit == 1:
            print("Valid")
        else:
            print("Corrupt")

countones(trial)
