hand = input("List all of the cards in your hand one after the other (i.e. 12345...: ")
blank = []
rejects = []
good_nums = []
for item in hand:
    blank.append(item)


def check_hand(cards):
    check_against = ['one', 'two', 'three', 'four', 'five']
    count = 0
    for item in cards:
        if int(item) == 1 and 'one' in check_against:
            count += 1
            check_against.remove('one')
            good_nums.append(item)
        elif int(item) == 2 and 'two' in check_against:
            count += 1
            check_against.remove('two')
            good_nums.append(item)
        elif int(item) == 3 and 'three' in check_against:
            count += 1
            check_against.remove('three')
            good_nums.append(item)
        elif int(item) == 4 and 'four' in check_against:
            count += 1
            check_against.remove('four')
            good_nums.append(item)
        elif int(item) == 5 and 'five' in check_against:
            count += 1
            check_against.remove('five')
            good_nums.append(item)
        else:
            rejects.append(item)
    if count >= 5:
        print("Bet")
    else:
        print("Fold")
    #print(good_nums)
    #print(rejects)
    #print(check_against)

check_hand(hand)
