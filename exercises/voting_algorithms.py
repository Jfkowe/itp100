def plurality(election):
    """
    Return the plurality winner among three candidates, 1, 2, and 3. The input
    election is a list of three tuples, so in a plurality election only the
    first element matters.

      >>> plurality([(1, 2, 3), (2, 3, 1), (2, 1, 3)])
      2
      >>> plurality([(3, 2, 1), (3, 2, 1), (2, 1, 3)])
      3
    """
    vote_totals = {1: 0, 2: 0, 3: 0} 
    for vote in election:
        vote_totals[vote[0]] += 1

    return max(vote_totals.items(), key=lambda x: x[1])[0]

def exhaustive(election):
    """
     >>> exhaustive([(1, 2, 3), (2, 3, 1), (2, 1, 3)])
     The canidate getting removed is 3

     >>> exhaustive([(3, 2, 1), (3, 1, 2), (2, 1, 3)])
     The canidate getting removed is 1
    """
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        vote_totals[vote[0]] += 1
    lowest =  min(vote_totals.items(), key=lambda x: x[1])[0]
    print(f"The canidate getting removed is {lowest}")

def primary12(election):
    """
     >>> primary12([(1, 2, 3), (3, 1, 2), (3, 1, 2), (1, 2, 3), (1, 3, 2), (3, 1, 2), (2, 1, 3)])
     1

     >>> primary12([(1, 2, 3), (1, 3, 2), (2, 3, 1), (3, 2, 1), (3, 1, 2)])
     3
    """
    vote_totals = {1: 0, 2: 0, 3: 0}
    vote_totals2 = {1: 0, 2: 0, 3:0}
    key_list = list(vote_totals.keys())
    key_list2 = list(vote_totals2.keys())
    for vote in election:
        vote_totals[vote[0]] += 1
    if vote_totals[1] > vote_totals[2]:
        #print("Canidate 1 had more votes than canidate 2")
        for vote in election:
            if vote[0] == 2:
                vote_totals[vote[1]] += 1
        comparison = vote_totals[1]
    else:
        #print(f"Canidate 2 had more votes than canidate 1")
        for vote in election:
            if vote[0] == 1:
                vote_totals[vote[1]] += 1
        comparison = vote_totals[2]
    val_list = list(vote_totals.values())
    #print(comparison)
    if comparison > vote_totals[3]:
        #print("And more votes than canidate 3")
        position = val_list.index(comparison)
        return key_list[position]
    else:
        #print(f"But canidate 3 had more votes than canidate {comparison}")
        return 3
def primary13(election):
    """
     >>> primary13([(1, 2, 3), (3, 1, 2), (3, 1, 2), (1, 2, 3), (1, 3, 2), (3, 1, 2), (2, 1, 3), (3, 1, 2)])
     3

     >>> primary13([(1, 2, 3), (1, 3, 2), (2, 3, 1), (3, 2, 1), (3, 1, 2), (1, 3, 2)])
     1
    """
    vote_totals = {1: 0, 2: 0, 3: 0}
    vote_totals2 = {1: 0, 2: 0, 3:0}
    key_list = list(vote_totals.keys())
    key_list2 = list(vote_totals2.keys())
    for vote in election:
        vote_totals[vote[0]] += 1
    if vote_totals[1] > vote_totals[3]:
        #print("Canidate 1 had more votes than canidate 2")
        for vote in election:
            if vote[0] == 3:
                vote_totals[vote[1]] += 1
        comparison = vote_totals[1]
    else:
        #print(f"Canidate 2 had more votes than canidate 1")
        for vote in election:
            if vote[0] == 1:
                vote_totals[vote[1]] += 1
        comparison = vote_totals[3]
    val_list = list(vote_totals.values())
    #print(comparison)
    if comparison > vote_totals[2]:
        #print("And more votes than canidate 3")
        position = val_list.index(comparison)
        return key_list[position]
    else:
        #print(f"But canidate 3 had more votes than canidate {comparison}")
        return 2
def primary23(election):
    """
     >>> primary23([(1, 2, 3), (3, 1, 2), (3, 1, 2), (1, 2, 3), (1, 3, 2), (3, 1, 2), (2, 1, 3)])
     1

     >>> primary23([(1, 2, 3), (1, 3, 2), (2, 3, 1), (3, 2, 1), (3, 1, 2)])
     3
    """
    vote_totals = {1: 0, 2: 0, 3: 0}
    vote_totals2 = {1: 0, 2: 0, 3:0}
    key_list = list(vote_totals.keys())
    key_list2 = list(vote_totals2.keys())
    for vote in election:
        vote_totals[vote[0]] += 1
    if vote_totals[2] > vote_totals[3]:
        #print("Canidate 1 had more votes than canidate 2")
        for vote in election:
            if vote[0] == 3:
                vote_totals[vote[1]] += 1
        comparison = vote_totals[2]
    else:
        #print(f"Canidate 2 had more votes than canidate 1")
        for vote in election:
            if vote[0] == 2:
                vote_totals[vote[1]] += 1
        comparison = vote_totals[3]
    val_list = list(vote_totals.values())
    #print(comparison)
    if comparison > vote_totals[1]:
        #print("And more votes than canidate 3")
        position = val_list.index(comparison)
        return key_list[position]
    else:
        #print(f"But canidate 3 had more votes than canidate {comparison}")
        return 1
if __name__ == "__main__":
    import doctest
    doctest.testmod()
