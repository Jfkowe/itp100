
What is ASCII? What are its limitations and why do we need to move beyond it?
ACSII is the standard character set that we use. Its limitations are that it is not very complex and is "not acceptable" anymore as the only set.  

Which function in Python will give you the numeric value of a character (and thus its order in the list of characters)?
the ord() function.

Dr. Chuck says we move from a simple character set to a super complex character set. What is this super complex character set called?
Unicode

Describe bytes in Python 3 as presented in the video. Do a little web searching to find out more about Python bytes. Share something interesting that you find.
bytes are how much data is held in each character kind of. Something interesting I found is that you can convert any data type into bytes using the byte() method, it just makes the output immutable. To get around that, you can use the bytearray() method.

Break down the process of using .encode() and .decode methods on data we send over a network connection.

Essentially, these methods allow a standard form of communication, by turning things into UTF-8 bytes when sending and decoding UTF-8 bytes when receiving. You can do things other than UTF-8 but you wouldn't want to.
