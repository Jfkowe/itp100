1. Indexing strings: Indexing strings is how you show which position each character inside the string is at. For his example, the "b" in banana is at [0]

2. Python function for character in a string: The len() function is used. For example, len(b) outputs 1.

3. Loop over characters in a string: The first way that he mentions is a while function: while index < len(str): for example. The other way is a for loop, for example, for letter in fruit: print(letter). 

4. Slicing: Using a sectio of a string instead of one character. str = 'Jack" so s[0:2] would be Ja. or s[2:4] gives you ck.  

5. Concatenation: Joining two strings together. 'hi' + 'b' outputs 'hib'

6. "in" as a logical operator: in checks of a certain character is in a string. For example, 'J' in 'Jack' would return True.

7. When you use a comparison operator with a string: It does compare them, although it can be a little unpredictable because it can be based on your keyset on your computer I think he said. The == operator does always check for equality. 

8. String Methods: string methods are built in functions to work with strings. For example, word = "HHH' wordLower = word.lower(), print(worldLower) outputs hhh. 

9. White Space: White apce is either spaces or non-printing characters 

10. Unicode: Unicode refers to the technology standard for the consistent encoding, representation, and handling of text expressed in writing systems. 
