#File Questions

1. For opening functions, Python uses the open() function. It takes arguments for file names and the mode to open the file in. It returns a file handle.

2. A file handle is like the variable that shows you kind of what the file is and how to get to it. The actual data is stored in the file itself.

3. \n is the new line character. The name is pretty self explanatory.

4. Dr. Chuck says the most common way to treat a file when it is opened is telling it to read line by line.

5. I do not think you could achieve the same result using slicing. It would remove the \n from the line itself, but not from the print statement, which .rstrip does.

6. First is the line.startswith('From: ') way, next is the not startswith('From: ') way which has a continue statement, finally is the using in to select lines way, such as if not '@uct.ac.za' in Line:

7. It uses the keywords try and except, which tests if the commnad is viable, and if it isn't it performs the code within the except block.  
