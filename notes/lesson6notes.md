1. The other three are Sequential, Conditional, Store and reuse. At least that's what I heard him say. 

2. The program that Dr. Chuck uses counts down to 0, and then prints blast off. It is a while loop.

3. Another word for looping that gets introduced during the video is iteration.

4. A broken loop is called an infinie loop. The problem with the loop he makes is that the n value doesn't decrease.

5. The statement that can be used to exit a loop is "break"

6. The other loop control statement introduced is "continue" and it moves the loop to the next iteration. 

7. While loops are indefinite loops. The next one that will be discussed is the definite loop (set number of iterations)

8. The for loop is introduced, and it iterates through a set number of times based on the range of the loop.

9. Loop idioms are patterns in the way that we construct loops. Examples are in: all the number ones. Largest number, average number, smallest number. Then the one with booleans. 
