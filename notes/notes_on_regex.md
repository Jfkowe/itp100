1. regular expressions are a way to write shortcuts on how you want to affect the text. They are a very powerful tool if you can get the hang of them. They originated a while ago, in the 1960's.

2. 

^ Matches the beginning of a line \n
$ Matches the end of a line \n
- Matches any character \n
\s Matches whitespace \n
\S Matches any non whitespace character \n
* Repeates a character zero or more times \n
* ? Repeates a character zero or more times (non-greedy) \n
+ Repeats a character one or more times \n
+? Repeats a character one or more times (non-greedy)
[aeiou] Matches a single character in the lsited set \n
[^XYZ] Matches a character not in the listed set \n
[a-z0-9] The set of characters can include a range \n 
( Indicates where a string extraction is to start \n
) Indicates when a string extraction is to end \n

3. 
