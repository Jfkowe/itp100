#Lesson 5 notes

1. How can we use Markdown and git to create nice looking documents under          revision control?

We can use Markdown to make nice looking documents by using it to control the   html conversion that goes on from the terminal to git. 

2. Functions as stored and reused patterns:

Functions act as a set of instructions, so that you don't have to type out what to do every single time, you can just call the function.

3. Writing functions in Python:

   You use the def keyword. Example:
 def coolFunction(x,y):
    print(x+y)
4. Calling the function provides an output. It is important to learn this so that you know what people are talking about.

5. Examples of built in functions: type(), sum(), next(), tuple(), int()


